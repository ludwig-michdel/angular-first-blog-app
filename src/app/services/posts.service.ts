import { Post } from '../models/Post.model';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { post } from 'selenium-webdriver/http';

@Injectable()
export class PostsService
{

  posts: Post[] = [];
  postSubject = new Subject<Post[]>();
  
  post1 = new Post('Un post à papa', 'il a une barre barbe');
  post2 = new Post("coucou", "salu");
  post3 = new Post("post3", "poste");

  constructor() {
    this.createNewPost(this.post1);
    this.createNewPost(this.post2);
    this.createNewPost(this.post3);
  }

  emitPosts() {
    this.postSubject.next(this.posts);
  }

  createNewPost(newPost: Post) {
    this.posts.push(newPost);
    this.emitPosts();
  }

  removePost(index: number) {
    this.posts.splice(index, 1);
    this.emitPosts();
  }

  downvotePost(index: number) {
    this.posts[index].loveIts--;
    this.emitPosts();
  }

  upvotePost(index: number) {
    this.posts[index].loveIts++;
    this.emitPosts();
  }

}
